import React from 'react';
import alebrije from '../resources/images/alebrije.png';
import styles from '../styles/styles.module.css'

export interface Props{
    readonly bootcampName : string;
}
export function HeaderComponent (props: Props){
    const {bootcampName}= props;
    return(
        <div className={styles.toolbar}>
            <img

                src={alebrije}
                style={{width: 40}}
            />
            <p>{bootcampName}</p>
        </div>
    );

}


