import React from 'react';
import philosophya from '../resources/images/philosophya.png';
import styles from '../styles/styles.module.css'

interface Props{
    readonly titlePhilosophy : string;
    readonly titleMission : string;
    readonly titleObjective : string;
    readonly titlePlan: string;
}
export function PhilosophyScreen (props: Props){
    const {titlePhilosophy, titleMission, titleObjective, titlePlan } = props;
    return(


    <div className={styles.page}>
        <img
            className={styles.img}
            width="295"
            height="243"
            src={philosophya}
        />
        <h1 className={styles.heading}>{titlePhilosophy}</h1>

        <h2 className={styles.title}>{titleMission}</h2>
        <p className={styles.content}>Training course for students or technicians who aspire to become professional
            software developers.</p>

        <h2 className={styles.title}>{titleObjective}</h2>
        <p className={styles.content}>Unlike a traditional technical course of online platforms, the added value is
            actually the transmission of professional experience, that is, not only learn how to program using
            frameworks, but during the course are shared: tips, experiences, experiences, motivation and the human
            aspect of what it takes to work in a professional company.
        </p>

        <h2 className={styles.title}>{titlePlan}</h2>
        <ul className={styles.content}>
            <li>52 lessons - each lesson lasts approximately ~1-2 hours, you learn technical/theoretical/practical
                issues.
            </li>
            <li>2 mini-projects linked - during the lessons in parallel.</li>
            <li>Mobile front-end project - react, js, es6, typescript - focus on functional programming.</li>
            <li>Back-end API project - springboot, API REST, hibernate, mysql - focus on MVC model.</li>
            <li>Effective communication - training to realize effective/efficient use of remote communication
                channels.
            </li>
            <li>Slack - team communication distribution via channels.</li>
            <li>Zoom - quality communication in conferences, active participation.</li>
            <li>Email - mail filtering for efficient response.</li>
            <li>Rituals - understanding and execution of rituals and meetings</li>
            <li>Standup - slack and zoom</li>
            <li>Backlog grooming</li>
            <li>Poker planning</li>
            <li>Mentoring and presentations</li>
            <li>PR reviews - tasks assigned throughout the lessons and mini-projects are meticulously reviewed
                generating timely feedback.
            </li>
            <li>Use of Attlassian Tools - jira, bitbucket, slack, confluence, email inbox.</li>
            <li>Written communication in English
                professional bonus - tips, phrases, development of critical thinking, emphasis on teamwork,
                providing added value from day 1 professional.
            </li>
            <li>Portfolio and end-of-course letter - built by the student and serves as a professional presentation
                to employers to apply for a position as a trainee, resident or junior dev.
            </li>
        </ul>

    </div>


);

}
