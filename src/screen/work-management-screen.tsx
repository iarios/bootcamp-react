import React from 'react';
import jira from '../resources/images/atlassian-jira-logo.png'
import bitbucket from '../resources/images/logos__Bitbucket.png'
import slack from '../resources/images/Slack-Logo.png'
import zoom from '../resources/images/zoom_image.png'
import styles from '../styles/styles.module.css'


interface Props{
    readonly titleWorkManagement : string;
}
export function WorkManagementcreen (props: Props){
    const {titleWorkManagement} = props;
    return(
        <div className={styles.page}>
            <h1 className={styles.heading}>{titleWorkManagement}</h1>
            <div>
                <a target="_blank" rel="noopener" href="https://www.atlassian.com/es/software/jira">
                    <img
                        width="200"
                        height="70"
                        src={jira}
                    />
                </a>
                <p className={styles.content}> A leading software development tool for agile teams, it is designed to enable
                    software team members to plan, monitor and release high-quality software.</p>
            </div>
            <div>
                <a target="_blank" rel="noopener" href="https://bitbucket.org/product/">
                    <img
                        width="295"
                        height="110"
                        src={bitbucket}
                    />
                </a>
                <p className={styles.content}>Git version control system, but in the cloud. A pull request is a useful feature
                    that allows one developer to notify another about the changes they made to an application in a
                    branch of their repository so that they can merge or merge if they choose to do so.
                    Bitbucket is a web-based hosting service for projects using the Mercurial and Git version control
                    system. It was launched in 2008 by the company Atlassian Software and is written in Python using the
                    Django web framework.
                    Bitbucket offers free and commercial accounts. The free ones have an unlimited number of private
                    repositories and five users, although they have the option of reaching a total of 8 if you invite 3
                    to join the service</p>
            </div>
            <div>
                <a target="_blank" rel="noopener" href="https://slack.com/intl/es-mx/">
                    <img
                        width="200"
                        height="100" src={slack}
                    />
                </a>
                <p className={styles.content}>It is a very useful messaging platform based on channels, to maintain good
                    communication with the work team, helping to improve the productivity of companies.</p>
            </div>
            <div>
                <a target="_blank" rel="noopener" href="https://zoom.us/">
                    <img
                        width="200"
                        height="100" src={zoom}
                    />
                </a>
                <p className={styles.content}>Zoom is the leader in modern enterprise video communications, with an easy,
                    reliable cloud platform for video and audio conferencing, chat, and webinars across mobile, desktop,
                    and room systems. Zoom Rooms is the original software-based conference room solution used around the
                    world in board, conference, huddle, and training rooms, as well as executive offices and
                    classrooms.</p>
            </div>
        </div>
    );

}
