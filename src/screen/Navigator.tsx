import React from 'react';
import { Link} from 'react-router-dom';
import styles from '../styles/styles.module.css'

export function Navigator() {
    return (
        <div className={styles.menu}>
        <ul>
            <li>
                <Link className={styles.active} to="/philosophy">Philophy</Link>
            </li>
            <li>
                <Link to="/workManagement">Work Management</Link>
            </li>
            <li>
                <Link to="/technologies">Technologies</Link>
            </li>
        </ul>
        </div>
    )
}
